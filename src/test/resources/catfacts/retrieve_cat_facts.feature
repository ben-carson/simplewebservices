@cat_facts
  Feature: retrieve cat facts
    Exercise the cat facts generator

  Scenario: Get a fact
    Given the controller is available
    When a fact is requested
    Then a non-zero length string is returned