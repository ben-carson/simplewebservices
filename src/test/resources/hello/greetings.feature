@hello
Feature: Greetings
  Exercise the greetings in this web service

  Scenario: Default Greeting
    Given Greeting is available
    When a greeting is requested without a name
    Then I should receive "Hello, World!"