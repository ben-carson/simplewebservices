package net.bencarson.tests.catfacts;

import net.bencarson.services.catfacts.CatFact;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TestCatFacts {

    private CatFact catFact;
    private int catFactId = 22;
    private String catFactText = "Cats are great!";

    @Before
    public void initTests() {
        catFact = new CatFact(catFactId,catFactText);
    }

    @Test
    public void TestGetFactId() {
        Assert.assertEquals(catFactId, catFact.getFactId());
    }

    @Test
    public void TestGetFactText() {
        Assert.assertEquals(catFactText,catFact.getFactText());
    }

}
