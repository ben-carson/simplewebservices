package net.bencarson.tests.catfacts;

import net.bencarson.services.catfacts.CatFactsController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestCatFactsController {

    CatFactsController catController;

    @Before
    public void SetUpControllerUnderTest() {
        catController = new CatFactsController();
    }

    @Test
    public void TestDefaultCatFactControllerConstructor() {
        Assert.assertNotNull(catController);
    }

    @Test
    public void TestReturnCatFact() {
        Assert.assertNotNull(catController.getCatFact());
        Assert.assertTrue(catController.getCatFact().getFactText().length() > 0 );
    }

}
