package net.bencarson.tests.numbers;

import net.bencarson.services.numbers.NumbersController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestNumbersController {

    NumbersController classUnderTest;

    @Before
    public void initNumbersController() {
        classUnderTest = new NumbersController();
    }

    @Test
    public void TestDefaultConstructor() {
        Assert.assertNotNull(classUnderTest);
    }

    @Test
    public void TestRollingAD20() {
        int testResult = classUnderTest.getD20();
        Assert.assertTrue(testResult > 0 && testResult <= 20);
    }

    @Test
    public void TestRollingAD4() {
        int testResult = classUnderTest.getRandomNumber(4);
        Assert.assertTrue(testResult > 0 && testResult <= 4);
    }

    @Test
    public void TestRollingAD6() {
        int testResult = classUnderTest.getRandomNumber(6);
        Assert.assertTrue(testResult > 0 && testResult <= 6);
    }

    @Test
    public void TestAddingTwoInt() {
        int testResult = classUnderTest.performAddition(1, 1);
        Assert.assertEquals(2, testResult);
    }

}
