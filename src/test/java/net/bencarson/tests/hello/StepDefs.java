package net.bencarson.tests.hello;

import cucumber.api.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefs {

    @Given("^Greeting is available$")
    public void greeting_is_available() {
        //TODO: implement this Step
        throw new PendingException("Not yet implemented.");
    }

    @When("^a greeting is requested without a name$")
    public void get_a_greeting_without_a_name() {
        //TODO: implement this Step
        throw new PendingException("Not yet implemented.");
    }

    @Then("^I should receive \"([^\"]*)\"$")
    public void i_should_received_a_greeting(String arg1) {
        //TODO: implement this Step
        throw new PendingException("Not yet implemented.");
    }
}
