package net.bencarson.tests;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(  monochrome = true,
        tags = "@tags",
        features = "src/test/resources/hello/",
        /*
        format = { "pretty","html: cucumber-html-reports",
                "json: cucumber-html-reports/cucumber.json" },
         */
        dryRun = false,
        glue = "net.bencarson.tests")
public class TestRunner {
}
