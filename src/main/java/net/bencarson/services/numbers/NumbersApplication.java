package net.bencarson.services.numbers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class NumbersApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(net.bencarson.services.numbers.NumbersApplication.class, args);
    }

}
