package net.bencarson.services.numbers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class NumbersController {

    private static Random rng = new Random();
    //private static int[] repeatBuffer = new int[2];

    /**
     * Just for fun, return a random integer, 0-19
     * @return int representing a random number between 0 and 19
     */
    @RequestMapping("/getd20")
    public int getD20() { return getRandomNumber(20); }

    @RequestMapping("/rolld/{uBound}")
    public int getRandomNumber(@PathVariable("uBound") int uBound) {
        //Since simulating dice rolls, add 1 to result since result is (0 to uperBound-1)
        return rng.nextInt(uBound) + 1;
    }

    /***
     * simple method to add two numbers together
     * @param add1 this is the first number to be added
     * @param add2 the second number to be added
     * @return an integer representing the sum of the two addends
     */
    @RequestMapping("/simpleadd/{addend1}/{addend2}")
    public int performAddition(@PathVariable("addend1") int add1,@PathVariable("addend2") int add2) {
        return add1+add2;
    }

}
