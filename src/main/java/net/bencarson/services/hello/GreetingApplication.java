package net.bencarson.services.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/*
Extended from SpringBootServletInitializer
Instructions from: https://www.baeldung.com/spring-boot-war-tomcat-deploy
 */
@SpringBootApplication
@ComponentScan(basePackageClasses=net.bencarson.services.hello.GreetingController.class)
public class GreetingApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(GreetingApplication.class, args);
    }
}
