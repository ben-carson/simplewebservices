package net.bencarson.services.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private Greeting returnGreeting(long id, String theName) {
        Greeting newGreeting = new Greeting(id, theName);
        return newGreeting;
    }

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam (value="name",defaultValue="World") String name ) {
        return returnGreeting(counter.incrementAndGet(), String.format(template, name));
    }

/*
    @RequestMapping("/error")
 */
    public Greeting errorMessage(@RequestParam (value="name", defaultValue="ERROR") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template,name));
    }
}
