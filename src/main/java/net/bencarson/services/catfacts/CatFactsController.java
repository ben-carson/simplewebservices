package net.bencarson.services.catfacts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Random;

@RestController
public class CatFactsController {

    private static Random rng = new Random();
    private static int[] repeatBuffer = new int[2];

    private static ArrayList<CatFact> catFacts = new ArrayList<CatFact>();

    public CatFact returnACatFact() {
        if(catFacts == null || catFacts.isEmpty()) {
            initCatFacts();
        }
        int newFactIndex = getFactIndex();  
        return catFacts.get(newFactIndex);
    }

    private int getFactIndex() {
        int factIndex = rng.nextInt(catFacts.size());
        if(repeatBuffer[0] == factIndex || repeatBuffer[1] == factIndex) {
            factIndex = getFactIndex();
        } else {
            repeatBuffer[0] = repeatBuffer[1];
            repeatBuffer[1] = factIndex;
        }
        return factIndex;
    }

    private void initCatFacts() {
        String[] theFacts =
                {"cats love laptops.",
                "cats sleep a lot.",
                "cats are territorial.",
                "cats like catnip.",
                "cats are obligate carnivores.",
                "Don't touch a cat's belly.",
                "Cats purr when happy or stressed.",
                "declawing a cat removes the cat's toes to the first knuckle."};
        //this is just a shitty method to merge facts and ids into an array
        for(int a = 0; a < theFacts.length-1; a++) {
            catFacts.add(new CatFact(a,theFacts[a]));
            System.out.println("adding catFacts["+a+"]:"+theFacts[a]);
        }
    }

    @RequestMapping("/getcatfact")
    public CatFact getCatFact() {
        CatFact catFact = new CatFact();
        catFact = this.returnACatFact();
        return catFact;
    }

}
