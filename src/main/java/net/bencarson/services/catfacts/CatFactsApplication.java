package net.bencarson.services.catfacts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//per this question, moving services under one package
//https://stackoverflow.com/a/36819544/185967
@SpringBootApplication
public class CatFactsApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(CatFactsApplication.class, args);
    }

}
