package net.bencarson.services.catfacts;

public class CatFact {

    private int factId;
    private String factText;

    //Constructors
    public CatFact() {
        //default constructor
    }
    public CatFact(int id, String fact) {
        this.setFactId(id);
        this.setFactText(fact);
    }

    // ACCESSORS
    public int getFactId() {
        return factId;
    }
    public void setFactId(int newFactId) {
        factId = newFactId;
    }

    public String getFactText() {
        return factText;
    }
    public void setFactText(String newFactText) {
        factText = newFactText;
    }
}
