# Simple Web Services

This is a project containing a series of simple web services. It is build with [Spring Boot](https://spring.io/projects/spring-boot) and [Maven](https://maven.apache.org/). I've also taken the step of using this project to incorporate my working with testing frameworks. I'm adding [JUnit](https://junit.org) and [Cucumber](https://cucumber.io) tests as I see fit.

Additionally, I am looking to use this project as a deployment artifact for my work with [AWS](https://aws.amazon.com/) and/or [Azure](https://azure.microsoft.com/en-us/). Just to have something fun to play with in the cloud.